// import 'package:demo_project/second.dart';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   String data;
//   var superheros_length;
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     getData();
//   }

//   void getData() async {
//     http.Response response = await http
//         .get("http://jhsoni71-001-site20.itempurl.com/api/Admin/GetOrders");
//     if (response.statusCode == 200) {
//       data = response.body; //store response as string
//       setState(() {
//         superheros_length = jsonDecode(
//             data)['Response']; //get all the data from json string superheros
//         print(superheros_length.length); // just printed length of data
//       });
// //      var venam = jsonDecode(data)['superheros'][4]['url'];
// //      print(venam);
//     } else {
//       print(response.statusCode);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Flutter Http Example"),
//       ),
//       body: ListView.builder(
//         itemCount: superheros_length == null ? 0 : superheros_length.length,
//         itemBuilder: (BuildContext context, int index) {
//           if (superheros_length == null) {
//             return Center(child: CircularProgressIndicator());
//           }
//           return GestureDetector(
//             onTap: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => SecondScreen(
//                     orderId:
//                         "${jsonDecode(data)['Response'][index]['OrderId']}",
//                     fullName: jsonDecode(data)['Response'][index]['FullName'],
//                   ),
//                 ),
//               );
//             },
//             child: Card(
//               child: ListTile(
//                 title: Text(
//                     "Order Id.${jsonDecode(data)['Response'][index]['OrderId']}"),
//                 subtitle: Text(jsonDecode(data)['Response'][index]['FullName']),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Flutter Demo',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Map<String, dynamic> post;
  List<dynamic> comments;

  bool _showLoading = true;
  bool _showComments = true;

  @override
  void initState() {
    super.initState();

    _fetchData();
  }

  Future _fetchData() async {
    setState(() {
      _showLoading = true;
    });

    final results = await Future.wait([
      http.get('http://jhsoni71-001-site20.itempurl.com/api/Admin/GetOrders'),
      http.get(
          'http://jhsoni71-001-site20.itempurl.com/api/Admin/GetOrderDetails?orderId=7504'),
    ]);

    setState(() {
      post = json.decode(results[0].body);
      comments = json.decode(
          results[1].body); //get all the data from json string superheros
      print(comments.length);
      _showLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("JSON Placeholder Blog post"),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(height: 70),
              if (_showLoading)
                Center(child: CupertinoActivityIndicator(animating: true)),
              if (!_showLoading) ...[
                Text(post['OrderId'],
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
                SizedBox(height: 10),
                _showCommentsToggle(),
                SizedBox(height: 10),
                if (!_showComments)
                  Text(post['Address1'], style: TextStyle(fontSize: 14)),
                if (_showComments) ..._commentList()
              ]
            ],
          ),
        ),
      ),
    );
  }

  _showCommentsToggle() {
    return Row(
      children: [
        Text(
          'Show comments',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        GestureDetector(
          child: CupertinoSwitch(
            value: _showComments,
            onChanged: (bool value) {
              setState(() {
                _showComments = value;
              });
            },
          ),
          onTap: () {
            setState(() {
              _showComments = !_showComments;
            });
          },
        ),
      ],
    );
  }

  _commentList() {
    return comments
        .map((comment) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  comment['FullName'],
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
                Text(comment['MobilNumber'], style: TextStyle(fontSize: 14)),
                SizedBox(height: 15)
              ],
            ))
        .toList();
  }
}
